using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Duplicate : MonoBehaviour
{
    //cr�e un vecteur avec tous les objets sauf le joueur
    public List<GameObject> all_objects_player;
    public List<GameObject> all_objects;
    public List<GameObject> all_copied_objects;
    public List<int> reference;
    //public List<Vector3> debug;
    public GameObject[] all_objects_player_array;
    public GameObject player;
    public GameObject mini;
   // public GameObject test;
    //public GameObject room;
    public Vector3 centre;
    public Vector3 scale = new Vector3(0.1f, 0.1f,0.1f);
    int i = 0;
    //var script = "OVR Grabbable";


    // Start is called before the first frame update
    void Start()
    {
        //Transform child = Selection.activeTransform;
        player = GameObject.Find("Player");
        all_objects_player_array = UnityEngine.Object.FindObjectsOfType<GameObject>();
        //reference.Add(i);
        //i++;

        foreach (GameObject go in all_objects_player_array)
        {
            all_objects_player.Add(go);
        }
        foreach (GameObject go in all_objects_player)
        {
            if (go != player && go!= GameObject.Find("MiniWorld") && go != GameObject.Find("Mini") && go != GameObject.Find("Directional Light"))

            {
                if (go.transform.IsChildOf(player.transform) != true)
                {
                    all_objects.Add(go);
                    //associer chaque object qui va etre dupliqu� au r�el avec sa position dans la liste
                    reference.Add(i);
                }
            }
            i++;
        }


        //cr�e un nouveau r�f�rentiel   //duplique tous les objets dans le nouveau referentiel
        mini.transform.position = new Vector3(0.19f, 0.1f, -1.3f);
        
        centre = mini.transform.position;

         int j = 0;
         foreach(GameObject go in all_objects) 
         {


            GameObject current_object_with_position;
            current_object_with_position = all_objects_player[reference[j]];
            Vector3 pos = current_object_with_position.transform.position;
            //debug.Add(pos);

            GameObject current_object = all_objects[j];
            GameObject copy_of_current_object=new GameObject();
            copy_of_current_object = Instantiate(current_object);//, new Vector3(0,0,0), Quaternion.identity
            copy_of_current_object.transform.position = centre + Vector3.Scale(pos, scale);
            copy_of_current_object.transform.rotation = current_object.transform.rotation;
            
            //centre+Vector3.Scale(pos, scale)

            if (copy_of_current_object.GetComponent<Rigidbody>() != null)
             {
                //Enlever la gravit�
                copy_of_current_object.GetComponent<Rigidbody>().useGravity = false;
                copy_of_current_object.GetComponent<Rigidbody>().isKinematic = true;
            }
            //Rescale
            copy_of_current_object.transform.localScale = Vector3.Scale(current_object.transform.localScale, scale);
            
           // copy_of_current_object.AddComponent<OVRGrabbable>();
            //copy_of_current_object.GetComponent<OVRGrabbable>().Initialize(current_object.GetComponent<Collider>());

            copy_of_current_object.transform.parent = mini.transform;
            all_copied_objects.Add(copy_of_current_object);

            j++;
            
                 }
         //Test with a unique GameObject
         /*
       test = Instantiate(GameObject.Find("room_01"));
        test.transform.position = new Vector3(0f, 0f, 0f);
        test.transform.rotation = GameObject.Find("room_01").transform.rotation;
        //test.GetComponent<Rigidbody>().useGravity = false;
        test.transform.localScale = Vector3.Scale(GameObject.Find("room_01").transform.localScale, scale);
         */


    }

     // Update is called once per frame
     void Update()
     {
        //Test with a unique GameObject
        /*
        test.transform.position = centre+ Vector3.Scale(GameObject.Find("room_01").transform.position, scale);
        test.transform.rotation = GameObject.Find("room_01").transform.rotation;*/

        /*// associe applique � chaque object miniature les transformations locales et globales des objects 
         centre = mini.transform.position;
         int k = 0;
          foreach (GameObject go in otherworld)
          {
              go.transform.position = centre + all_objects_player[reference[k]].transform.position;
          }
          k++;*/

        
        int k = 0;
        foreach (GameObject go in all_copied_objects)
        {


           Vector3 pos = all_objects_player[reference[k]].transform.position;
           go.transform.position = mini.transform.position + Vector3.Scale(pos, scale);
           go.transform.rotation = mini.transform.rotation ;
            go.transform.rotation = all_objects[k].transform.rotation;
            //Affect the children in hierarchy 
            //void OnCollisionEnter(Collision collision)
            /*{
                if (go == collision.gameObject)
                {
                    mini.transform.position = go.transform.position - centre + Vector3.Scale(pos, scale);
                    mini.transform.rotation = go.transform.rotation;
                }
            }*/

            k++;
        }
        
        //test for a table
    }

}