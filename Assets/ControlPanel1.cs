using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class ControlPanel1 : MonoBehaviour



{
    public GameObject myobjectcanva; 
    public Canvas mycanva;
    public Button buttonR;
    public Button buttonL;
    public Slider slider;
    //Transform mystransform = myobjectcanva.transform;
    int count=1;
  

    RectTransform rectTransform;
    // Start is called before the first frame update
    void Start()
    {
        Transform mystransform = myobjectcanva.transform;
        mycanva.transform.parent = myobjectcanva.transform;
        myobjectcanva.SetActive(false);
        mycanva.renderMode = RenderMode.WorldSpace;
        rectTransform = mycanva.GetComponent<RectTransform>();

        float x = rectTransform.eulerAngles.x;
        float y = rectTransform.eulerAngles.y;
        float z = rectTransform.eulerAngles.z;
        x = -10;
        y = 45;
        z = 0;
        //rectTransform.rotation.eulerAngles.x= new Vector3(-10, 45, 0);
        rectTransform.localPosition = new Vector3(myobjectcanva.transform.position.x, myobjectcanva.transform.position.y+1.5f, myobjectcanva.transform.position.z);
        rectTransform.sizeDelta = new Vector2(1, 1);

        buttonR.GetComponent<RectTransform>().sizeDelta= new Vector2(0.5f, 0.5f);
        //buttonR.GetComponent<RectTransform>().Height = 0.5;
        buttonL.GetComponent<RectTransform>().sizeDelta= new Vector2(0.5f, 0.5f);
        //buttonL.GetComponent<RectTransform>().Height = 0.5;
        buttonL.GetComponent<RectTransform>().localPosition= new Vector3(0.3f, buttonL.GetComponent<RectTransform>().localPosition.y, buttonL.GetComponent<RectTransform>().localPosition.z)  ;
        buttonR.GetComponent<RectTransform>().localPosition = new Vector3(0.3f, buttonR.GetComponent<RectTransform>().localPosition.y, buttonR.GetComponent<RectTransform>().localPosition.z);


        buttonR.transform.parent = mycanva.transform;
        buttonL.transform.parent = mycanva.transform;
        slider.transform.parent = mycanva.transform;

        slider.maxValue = 100;
        slider.minValue = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (OVRInput.Get(OVRInput.RawButton.RIndexTrigger))
        {
            count++;
            
        }
        if (count % 2 == 0 && count>2)
        {
            myobjectcanva.SetActive(true);

        }else myobjectcanva.SetActive(false);

        buttonR.onClick.AddListener(RotateR);
        buttonL.onClick.AddListener(RotateL);
        slider.onValueChanged.AddListener(Resize);





    }
    void RotateR()
    {
        myobjectcanva.transform.Rotate(myobjectcanva.transform.up, 20, Space.Self);

    }
    void RotateL()
    {
        myobjectcanva.transform.Rotate(myobjectcanva.transform.up, -20, Space.Self);

    }

    void Resize()
    {
        float value = 1 / slider.value;
        myobjectcanva.transform.localScale = Vector3.Scale(new Vector3(myobjectcanva.transform.position.x, myobjectcanva.transform.position.y, myobjectcanva.transform.position.x), value);

    }

}
